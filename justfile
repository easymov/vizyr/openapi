codegen := "docker run --rm -u $(id -u ${USER}):$(id -g ${USER}) -v ${PWD}:/local -w /local openapitools/openapi-generator-cli:v4.1.1"
python := "docker run --rm -v ${PWD}/package:/local -w /local python:3.6-alpine"

generator generator="python" package_name="vizyr_server":
    {{codegen}} generate \
        --enable-post-process-file \
        --generate-alias-as-model \
        -i /local/reference/vizyr/openapi.yaml \
        -g {{generator}} \
        -o /local/packages/{{generator}} \
        --package-name {{package_name}}
